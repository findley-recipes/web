FROM node:12-alpine as build

WORKDIR /opt/recipe

COPY package*.json ./

RUN npm ci

COPY . .

RUN npm run build

FROM nginx:alpine

WORKDIR /usr/share/nginx/html

COPY --from=build /opt/recipe/build .
COPY nginx.conf /etc/nginx/nginx.conf
