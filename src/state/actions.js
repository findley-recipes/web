import { push, replace } from 'connected-react-router';
import { logout } from '../services/authentication';

export default {
    login: token => ({
        type: 'LOGIN',
        token
    }),
    goToRecipe: id => dispatch => {
        dispatch(push(`/recipes/${id}`));
    },
    logout: () => {
        logout();
        return {type: 'LOGOUT'}
    },
    startSearch: (q = '') => dispatch => {
        dispatch(push(`/search/${q}`));
    },
    updateSearch: q => dispatch => {
        dispatch(replace(`/search/${q}`));
    },
    clearSearch: () => dispatch => {
        dispatch(push('/'));
    }
};
