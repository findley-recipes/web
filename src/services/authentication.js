const API_URL = process.env.REACT_APP_API_URL;

export function authenticated() {
    const token = getLocalToken();
    return token === null ? false : true;
}

export function authenticate(password) {
    return fetch(`${API_URL}/authenticate/`, {
        method: 'POST',
        headers: {
            'Content-type': 'application/json'
        },
        body: JSON.stringify({password})
    })
    .then(response => {
        if (response.status < 200 || response.status >= 400) {
            throw response
        }
        return response.json();
    })
    .then(response => {
        window.localStorage.setItem('token', JSON.stringify(response.token));
        return response.token;
    });
}

export function logout() {
    window.localStorage.removeItem('token');
}

export function getLocalToken() {
    const tokenString = window.localStorage.getItem('token');
    if (!tokenString) {
        return null;
    }

    let token;
    try {
        token = JSON.parse(tokenString);
    } catch(e) {
        return null;
    }

    if (Date.now() > token.exp) {
        return null;
    }

    return token;
}
