export function parseSearch(path) {
    let q = '';
    const searching = path.startsWith('/search');

    if (searching) {
        const pathParts = path.split('/');
        if (pathParts.length >= 2) {
            q = pathParts[pathParts.length - 1];
        }
    }

    return {q, searching}
}
