import { getLocalToken } from '../services/authentication';

const API_URL = process.env.REACT_APP_API_URL;

export function fetchRecipe(id) {
    return fetch(`${API_URL}/recipes/${id}`)
    .then(response => {
        if (response.status >= 400) {
            throw response;
        }
        return response.json();
    })
}

export function listRecipes(q, page = 1, limit = 30) {
    let resultCount;

    return fetch(`${API_URL}/recipes?q=${q}&page=${page}&limit=${limit}`)
    .then(response => {
        if (response.status >= 400) {
            throw response;
        }
        resultCount = response.headers.get('X-Result-Count') || 30;
        return response.json();
    })
    .then(result => [result, resultCount]);
}

export function saveRecipe(recipe) {
    const token = getLocalToken();
    let url = `${API_URL}/recipes/`;
    let method = 'POST';
    if (recipe._id && recipe.linkName) {
        url = `${API_URL}/recipes/${recipe.linkName}`;
        method = 'PATCH';
    }

    return fetch(url, {
        method,
        headers: {
            'Content-type': 'application/json',
            'Authorization': `Bearer ${token}`
        },
        body: JSON.stringify(recipe)
    })
    .then(response => {
        if (response.status >= 400) {
            throw response;
        }
        return response.json();
    });
}
