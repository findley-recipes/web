const API_URL = process.env.REACT_APP_API_URL;

export function fetchDash() {
    return fetch(`${API_URL}/dash`)
    .then(response => {
        if (response.status >= 400) {
            throw response;
        }
        return response.json()
    });
}
