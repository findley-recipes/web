import React from 'react';
import { connect } from 'react-redux';
import { withStyles, withTheme } from '@material-ui/core/styles';
import {AppBar as MuiAppBar, Toolbar, Typography, Button, IconButton, Hidden} from '@material-ui/core';
import { Link } from 'react-router-dom';
import { withRouter } from 'react-router';
import AppSearchBar from './AppSearchBar';

import { parseSearch } from '../../services/search';
import ArrowBack from '@material-ui/icons/ArrowBack';
import SearchIcon from '@material-ui/icons/Search';
import actions from '../../state/actions';

const styles = theme => ({
    root: {
        flexGrow: 1,
        marginBottom: theme.spacing.unit * 12,
        [theme.breakpoints.down('xs')]: {
            marginBottom: theme.spacing.unit * 9,
        }
    },
    toolbarRoot: {
        justifyContent: 'space-between'
    },
    titleBar: {
        marginRight: theme.spacing.unit * 2
    },
    searchBar: {
        flexGrow: 1,
    },
    rightBar: {
        marginLeft: theme.spacing.unit * 5,
        [theme.breakpoints.down('xs')]: {
            marginLeft: theme.spacing.unit
        }
    },
    titleText: {
        fontFamily: '"Courgette", cursive',
        marginRight: theme.spacing.unit * 3,
        color: 'white',
        [theme.breakpoints.down('xs')]: {
            marginRight: 0
        }
    },
});

class AppBar extends React.Component {
    constructor(props) {
        super(props);
        this.handleSearchIconClick = this.handleSearchIconClick.bind(this);
        this.clearSearch = this.clearSearch.bind(this);
    }

    render() {
        const {classes, searching} = this.props;
        const {primary: primaryColor} = this.props.theme.palette;

        return (
            <div className={classes.root}>
                <MuiAppBar
                    position="fixed"
                    style={{
                        backgroundColor: searching ? '#eea5a5' : primaryColor.main,
                        color: searching ? '#222222' : primaryColor.contrastText
                    }} >
                    <Toolbar classes={{root: classes.toolbarRoot}}>
                        {this.renderTitle(classes)}
                        {this.renderSearch(classes)}
                        {this.renderRightBar(classes)}
                    </Toolbar>
                </MuiAppBar>
            </div>
        );
    }

    renderTitle(classes) {
        if (this.props.searching) {
            return (
                <div className={classes.titleBar}>
                    <IconButton onClick={this.clearSearch}>
                        <ArrowBack />
                        <Hidden implementation="css" xsDown>
                            <Typography type="title" color="inherit">
                                Back
                            </Typography>
                        </Hidden>
                    </IconButton>
                </div>
            );
        }

        return (
            <Link to="/">
                <div className={classes.titleBar}>
                    <Typography noWrap className={classes.titleText} variant="title">
                        Findley Recipes
                    </Typography>
                </div>
            </Link>
        );
    }

    renderSearch(classes) {
            const searchBarHiddenProps = {
                xsDown: !this.props.searching
            };
            return (
                <Hidden {...searchBarHiddenProps} implementation="css" className={classes.searchBar}>
                    <AppSearchBar />
                </Hidden>
            );
    }

    renderRightBar(classes) {
        if (this.props.searching) {
            return;
        }
        return (
            <div className={classes.rightBar}>
                <Hidden smUp>
                    <IconButton color="inherit" onClick={this.handleSearchIconClick}>
                        <SearchIcon />
                    </IconButton>
                </Hidden>
                {this.props.authenticated ? (
                    <Button variant="outlined" color="inherit" onClick={() => this.props.dispatch(actions.logout())}>Logout</Button>
                ) : (
                    <Link to="/login"><Button variant="outlined" color="inherit">Login</Button></Link>
                )}
            </div>
        )
    }

    clearSearch() {
        this.props.dispatch(actions.clearSearch());
    }

    handleSearchIconClick() {
        this.props.dispatch(actions.startSearch());
    }
}

const mapStateToProps = state => ({
    searching: parseSearch(state.router.location.pathname).searching
});

export default connect(mapStateToProps)(withRouter(withTheme()(withStyles(styles)(AppBar))));
