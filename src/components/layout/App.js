import React from 'react';
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';
import {Button} from '@material-ui/core';
import { ConnectedRouter as Router } from 'connected-react-router';
import {Link, Route, Switch, Redirect} from 'react-router-dom';
import AddIcon from '@material-ui/icons/Add';
import AppBar from './AppBar';
import Content from '../utils/Content.jsx';
import history from '../../state/history';
import pages from '../pages';

const styles = theme => ({
    addButton: {
        position: 'fixed',
        bottom: theme.spacing.unit * 2,
        right: theme.spacing.unit * 2,
        zIndex: 1000
    }
});

const AddButton = props => (
    <Link to="/new">
        <Button variant="fab" color="primary" aria-label="Add" className={props.classes.addButton}>
            <AddIcon />
        </Button>
    </Link>
);

const App = props => {
    return (
        <Router history={history}>
            <div>
                <div className="no-print">
                    <AppBar authenticated={props.authenticated} />
                    {props.authenticated ? (
                        <AddButton classes={props.classes} />
                    ) : null}
                </div>
                <Content>
                    <Switch>
                        <Route exact path="/" component={pages.Home} />
                        <Route path="/recipes/:id" component={pages.Recipe} />
                        <Route path="/new" component={pages.New} />
                        <Route path="/search/:q?" component={pages.Search} />
                        <Route path="/browse" component={pages.Browse} />
                        {props.authenticated ? (
                            <Redirect from="/login" to="/" />
                        ) : (
                            <Route path="/login" component={pages.Login} />
                        )}
                        <Route path="/404" component={pages.NotFound} />
                    </Switch>
                </Content>
            </div>
        </Router>
    );
};

export default connect(state => ({authenticated: state.authenticated}))(withStyles(styles)(App));
