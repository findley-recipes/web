import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import {
    Paper,
    Typography,
} from '@material-ui/core';
import BigList from './BigList';
import RecipeListItem from '../recipe-list/RecipeListItem';
import RecipeListItemLoader from '../recipe-list/RecipeListItemLoader';
import { listRecipes } from '../../services/recipe';

const styles = theme => ({
    root: {},
    header: {
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginBottom: 0,
    },
    title: {
        marginBottom: theme.spacing.unit,
        marginLeft: theme.spacing.unit * 2,
        [theme.breakpoints.down('xs')]: {
            marginLeft: theme.spacing.unit
        }
    }
});

const BigRecipeList = props => {
    const {classes, children, header, q} = props;
    return (
        <div className={classes.root}>
            <div className={classes.header}>
                <Typography variant="headline" className={classes.title}>{header}</Typography>
                <div>
                    {children}
                </div>
            </div>
            <Paper>
                <BigList
                    key={`big-list-${q}`}
                    rowHeight={68}
                    initialCount={30}
                    getItems={(offset, limit) => listRecipes(q, Math.floor(offset/limit)+1, limit)}
                    renderRow={(recipe) => (
                        <RecipeListItem recipe={recipe} />
                    )}
                    renderLoadingRow={() => (
                        <RecipeListItemLoader />
                    )} />
            </Paper>
        </div>
    );
};

export default withStyles(styles)(BigRecipeList);
