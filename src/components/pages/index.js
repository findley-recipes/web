import Home from './HomePage';
import Search from './SearchPage';
import Recipe from './RecipePage';
import New from './NewRecipePage';
import Login from './LoginPage';
import NotFound from './NotFoundPage';
import Browse from './BrowsePage';

export default {
    Home,
    Search,
    Recipe,
    New,
    Login,
    NotFound,
    Browse,
};
