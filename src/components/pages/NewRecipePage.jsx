import React from 'react';
import RecipeEditor from '../recipe-editor/RecipeEditor';
import { saveRecipe } from '../../services/recipe';

export default class NewRecipePage extends React.Component {
    constructor(props) {
        super(props);

        this.handleSave = this.handleSave.bind(this);
        this.handleCancel = this.handleCancel.bind(this);
    }

    render() {
        return (
            <RecipeEditor
                onCancel={this.handleCancel}
                onSave={this.handleSave}
            />
        );
    }

    handleCancel() {
        const {history} = this.props;
        history.push('/');
    }

    handleSave(recipe) {
        saveRecipe(recipe)
        .then(r => {
            const {history} = this.props;
            history.push(`/recipes/${r.linkName}`, r);
        })
        .catch(response => {
            if (response.status === 409) {
                alert('A recipe with this name and author already exists');
            } else {
                alert('Failed to save recipe');
            }
        });
    }
};
