import React from 'react';
import { connect } from 'react-redux';
import SearchIcon from '@material-ui/icons/Search';
import { withStyles } from '@material-ui/core/styles';
import { parseSearch } from '../../services/search';
import RecipeList from '../recipe-list/RecipeList';
import { listRecipes } from '../../services/recipe';

const styles = theme => ({
    emptyRoot: {
        display: 'flex',
        justifyContent: 'center',
        height: '70vh'
    },
    searchWatermark: {
        fontSize: 350,
        color: '#b9b9b9'
    }
});

class SearchPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            results: []
        };
        this.fetchingData = false;
        this.lastSearch = '';
        this.updateTimeout = null;
    }

    render() {
        const {classes, q} = this.props;

        if (q === '') {
            return (
                <div className={classes.emptyRoot}>
                    <SearchIcon className={classes.searchWatermark} />
                </div>
            );
        }

        if (this.state.results.length === 0) {
            return (
                <div className={classes.emptyRoot}>
                    <p>No results</p>
                </div>
            );
        }

        return (
            <div>
                <RecipeList header="Results" recipes={this.state.results} />
            </div>
        );
    }

    componentDidUpdate() {
        if (this.updateTimeout) {
            clearTimeout(this.updateTimeout);
        }

        this.updateTimeout = setTimeout(() => {
            this.fetchData();
            this.updateTimeout = null;
        }, 500);
    }

    componentDidMount() {
        const {location} = this.props;

        if (location.state !== undefined) {
            this.setState({results: location.state})
        } else {
            this.fetchData();
        }
    }

    fetchData() {
        const q = this.props.q;
        if (q === this.lastSearch) {
            return;
        }

        this.lastSearch = q;
        this.fetchingData = true;
        listRecipes(q)
        .then(([recipes]) => {
            this.setState({results: recipes});
            this.fetchingData = false;
        });
    }
};

const mapStateToProps = state => ({
    q: parseSearch(state.router.location.pathname).q
});

export default connect(mapStateToProps)(withStyles(styles)(SearchPage));
