import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import BigRecipeList from '../BigRecipeList/BigRecipeList';

const styles = theme => ({
    recipeList: {
        marginBottom: theme.spacing.unit * 5
    }
});

class BrowsePage extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            q: '',
        };
    }

    render() {
        const {recipeList} = this.props.classes;
        const { q } = this.state;

        return (
            <div>
                <input value={q} onChange={e => this.setState({q: e.target.value})} />
                <BigRecipeList q={q} key="hello" header="All Recipes" classes={{root: recipeList}} />
            </div>
        );
    }
}

export default withStyles(styles)(BrowsePage);
