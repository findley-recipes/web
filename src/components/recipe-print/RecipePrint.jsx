import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import { Grid, Button } from '@material-ui/core';
import { Link } from 'react-router-dom';
import classNames from 'classnames';
import { connect } from 'react-redux';

import actions from '../../state/actions';

class RecipePrint extends React.Component {
    render() {
        const {classes, recipe} = this.props;

        return (
            <div className={classes.root}>
                <Grid container spacing={8}>
                    <Grid item xs={2} className={classes.rightText}>Title:</Grid>
                    <Grid item xs={10}>{recipe.name}</Grid>
                </Grid>

                <Grid container spacing={8}>
                    <Grid item xs={2} className={classes.rightText}>Author:</Grid>
                    <Grid item xs={10}>{recipe.author}</Grid>
                </Grid>

                <Grid container spacing={8}>
                    <Grid item xs={2} className={classes.rightText}>Tags:</Grid>
                    <Grid item xs={10}>{recipe.tags.join(', ')}</Grid>
                </Grid>

                <Grid container spacing={8}>
                    <Grid item xs={2} className={classes.rightText}>Servings:</Grid>
                    <Grid item xs={10}>{recipe.yield}</Grid>
                </Grid>

                {this.renderIngredients()}
                {this.renderInstructions()}

                <div className={classNames('no-print', classes.backButton)}>
                    <Link to={`/recipes/${recipe.linkName}`}>
                        <Button>Back</Button>
                    </Link>
                </div>
            </div>
        );
    }

    renderIngredients() {
        const {classes, recipe} = this.props;

        return recipe.ingredients.map((section, i) => {
            const ingredients = section.list.map((ingredient, j) => (
                <Grid item xs={6} key={j}>
                    <span className={classes.ingQty}>{ingredient.qty}</span>
                    <span className={classes.ingUnit}>{ingredient.unit}</span>
                    <span>{ingredient.name}</span>
                </Grid>
            ));

            return (
                <div className={classes.ingredientSection} key={i}>
                    {section.name ? <div><strong>{section.name}</strong></div> : null}
                    <Grid container>
                        {ingredients}
                    </Grid>
                </div>
            );
        });
    }

    renderInstructions() {
        const {classes, recipe} = this.props;

        return recipe.instructions.map((section, i) => {
            return (
                <div className={classes.instructionSection} key={i}>
                    {section.name ? <div><strong>{section.name}</strong></div> : null}
                    {section.content.split('\n').map((content, j) => (
                        <p className={classes.instructionStep} key={j}>{content ? content : '\u00A0'}</p>
                    ))}
                </div>
            );
        });
    }

    componentDidMount() {
        window.onafterprint = () => this.props.dispatch(actions.goToRecipe(this.props.recipe.linkName));
        window.print();
    }

    componentWillUnmount() {
        window.onafterprint = undefined;
    }
};

const styles = theme => ({
    root: {
        fontSize: '1.1rem',
        fontFamily: 'monospace',
        padding: theme.spacing.unit
    },
    rightText: {
        textAlign: 'right'
    },
    tag: {
        marginRight: theme.spacing.unit
    },
    ingredientSection: {
        marginTop: theme.spacing.unit * 2
    },
    ingQty: {
        minWidth: 40,
        display: 'inline-block'
    },
    ingUnit: {
        minWidth: 40,
        display: 'inline-block'
    },
    instructionSection: {
        marginTop: theme.spacing.unit * 2
    },
    instructionStep: {
        margin: 0
    },
    backButton: {
        position: 'fixed',
        bottom: theme.spacing.unit * 2,
        left: theme.spacing.unit * 2,
        zIndex: 1000
    }
});

export default connect()(withStyles(styles)(RecipePrint));
