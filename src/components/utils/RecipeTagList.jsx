import React from 'react';
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';
import { Chip } from '@material-ui/core';

import actions from '../../state/actions';

const styles = theme => ({
    root: {
        display: 'flex',
        justifyContent: 'center'
    },
    chipRoot: {
        height: 23,
        margin: theme.spacing.unit
    }
});

const RecipeTag = connect()(props => {
    const handleClick = props.link ? () => props.dispatch(actions.startSearch(props.label)) : null;
    return (
        <Chip
            label={props.label}
            color="primary"
            classes={{root: props.classes.chipRoot}}
            onClick={handleClick}
        />
    )
});

const RecipeTagList = props => {
    const tags = props.tags.map((label, i) => (
        <RecipeTag label={label} classes={props.classes} key={i} link={props.link} />
    ));

    return (
        <div className={props.classes.root}>
            {tags}
        </div>
    );
};

export default withStyles(styles)(RecipeTagList);
