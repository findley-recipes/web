import React from 'react';
import PropTypes from 'prop-types';
import './shimmer-loader.css';

const Shimmer = props => {
    const { width, height, radius } = props;

    return (
        <div
            style={{width: `${width}px`, height: `${height}px`, borderRadius: `${radius}px`}}
            className="shimmer-loader">
        </div>
    );
};

Shimmer.propTypes = {
    width: PropTypes.number.isRequired,
    height: PropTypes.number.isRequired,
    radius: PropTypes.number,
};

Shimmer.defaultProps = {
    radius: 0,
};

export default Shimmer;
