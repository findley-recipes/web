import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import { Grid } from '@material-ui/core';

const styles = theme => ({
    root: {
        paddingLeft: theme.spacing.unit * 2,
        paddingRight: theme.spacing.unit * 2,
        marginBottom: theme.spacing.unit * 6,
        [theme.breakpoints.down('xs')]: {
            paddingLeft: 0,
            paddingRight: 0,
        }
    }
});

const Content = (props) => (
    <Grid container justify="center" spacing={0} >
        <Grid item xs={12} sm={12} md={10} lg={8} xl={8} className={props.classes.root}>
            {props.children}
        </Grid>
    </Grid>
);

export default withStyles(styles)(Content);
