import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import {
    Menu,
    MenuItem,
} from '@material-ui/core';
import CategoryIcon, { iconMap } from '../utils/CategoryIcon';

const styles = theme => ({
    root: {
    },
    button: {
        display: 'flex',
        cursor: 'pointer'
    }
});

class CategoryIconPicker extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            anchor: null
        };

        this.handleClick = this.handleClick.bind(this);
        this.handleClose = this.handleClose.bind(this);
        this.handleSelect = this.handleSelect.bind(this);
    }

    render() {
        const {classes, value} = this.props;
        const open = Boolean(this.state.anchor);

        return (
            <div>
                <div className={classes.button} onClick={this.handleClick}>
                    <CategoryIcon iconName={value} />
                </div>
                <Menu
                    anchorEl={this.state.anchor}
                    open={open}
                    onClose={this.handleClose}
                    PaperProps={{
                        style: {
                            maxHeight: 300
                        }
                    }}
                >
                    {Object.keys(iconMap).map((key, i) => (
                        <MenuItem onClick={() => this.handleSelect(key)} key={i}>
                            <CategoryIcon iconName={key} />
                        </MenuItem>
                    ))}
                </Menu>
            </div>
        );
    }

    handleClick(event) {
        this.setState({anchor: event.currentTarget});
    }

    handleSelect(value) {
        this.setState({anchor: null});
        if (this.props.onChange) {
            this.props.onChange(value);
        }
    }

    handleClose() {
        this.setState({anchor: null});
    }
}

export default withStyles(styles)(CategoryIconPicker);
