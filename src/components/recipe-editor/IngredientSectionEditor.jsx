import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import {
    TextField,
    Button
} from '@material-ui/core';
import IngredientEditor from './IngredientEditor';

import emptyIngredient from '../../services/emptyIngredient';

const IngredientSectionEditor = class IngredientSectionEditor extends React.Component {
    constructor(props) {
        super(props);
        this.handleEditIngredient = this.handleEditIngredient.bind(this);
        this.handleRemoveIngredient = this.handleRemoveIngredient.bind(this);
        this.nextIngredient = emptyIngredient();
    }

    render() {
        const {ingredients, classes} = this.props;

        let ingredientEditors = ingredients.list.map((ingredient, i) => (
            <IngredientEditor
                onChange={ing => this.handleEditIngredient(ing, i)}
                onRemove={() => this.handleRemoveIngredient(i)}
                ingredient={ingredient}
                key={ingredient._id}
            />
        ));

        ingredientEditors.push(
            <IngredientEditor
                onChange={ing => this.handleEditIngredient(ing)}
                removeable={false}
                ingredient={this.nextIngredient}
                key={this.nextIngredient._id}
            />
        );

        return (
            <div className={classes.root}>
                {this.renderNameInput()}
                {ingredientEditors}
                <div className={classes.footer}>
                    {this.renderRemoveButton()}
                </div>
            </div>
        );
    }

    renderNameInput() {
        const {ingredients, classes, removeable} = this.props;

        if (!removeable) {
            return;
        }

        return (
            <TextField
                fullWidth={true}
                defaultValue={ingredients.name}
                className={classes.sectionNameInput}
                InputLabelProps={{className: classes.sectionNameLabel}}
                label="Ingredient Section"
                margin="normal"
                onChange={event => this.props.onChange({name: event.target.value})}
            />
        );
    }

    renderRemoveButton() {
        const {removeable} = this.props;

        if (!removeable) {
            return;
        }

        return (
            <Button onClick={this.props.onRemove}>
                Remove Section
            </Button>
        );
    }

    handleRemoveIngredient(i) {
        const n = [
            ...this.props.ingredients.list.slice(0, i),
            ...this.props.ingredients.list.slice(i+1)
        ];

        this.props.onChange({
            list: n
        });
    }

    handleEditIngredient(updatedIngredient, i) {
        let newList = this.props.ingredients.list.map((ingredient, index) => {
            if (i !== index) {
                return ingredient;
            }

            return updatedIngredient;
        });

        if (i === undefined) {
            newList.push(updatedIngredient);
            this.nextIngredient = emptyIngredient();
        }

        this.props.onChange({
            list: newList
        });
    }
};

const styles = theme => ({
    root: {
        border: 'solid #ababab 1px',
        borderRadius: 8,
        padding: 10,
        marginBottom: theme.spacing.unit * 3,
        marginTop: theme.spacing.unit * 2
    },
    footer: {
        display: 'flex',
        justifyContent: 'flex-end'
    },
    sectionNameInput: {
        marginTop: -18
    },
    sectionNameLabel: {
        backgroundColor: 'white',
        padding: '0 4px'
    }
});

export default withStyles(styles)(IngredientSectionEditor);
