import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import {
    TextField,
    IconButton
} from '@material-ui/core';
import DeleteIcon from '@material-ui/icons/Delete';


const IngredientEditor = class IngredientEditor extends React.Component {
    constructor(props) {
        super(props);
        this.handleChange = this.handleChange.bind(this);
    }

    render() {
        const {classes, ingredient} = this.props;
        return (
            <div className={classes.root}>
                <TextField defaultValue={ingredient.qty} name="qty" onChange={this.handleChange} placeholder="Quantity" className={classes.quantity} margin="dense" />
                <TextField defaultValue={ingredient.unit} name="unit" onChange={this.handleChange} placeholder="Units" className={classes.units} margin="dense" />
                <div className={classes.name}>
                    <TextField defaultValue={ingredient.name} name="name" onChange={this.handleChange} placeholder="Name" fullWidth={true} margin="dense" />
                </div>
                <IconButton aria-label="Delete" size="small" onClick={this.props.onRemove} disabled={!this.props.removeable}>
                    <DeleteIcon fontSize="small" />
                </IconButton>
            </div>
        );
    }

    handleChange(event) {
        this.props.onChange({
            ...this.props.ingredient,
            [event.target.name]: event.target.value
        });
    }
};

const styles = theme => ({
    root: {
        display: 'flex'
    },
    name: {
        flexGrow: 1
    },
    quantity: {
        marginRight: theme.spacing.unit * 2,
        maxWidth: 150,
        minWidth: 40
    },
    units: {
        marginRight: theme.spacing.unit * 2,
        maxWidth: 150,
        minWidth: 40
    }
});

IngredientEditor.defaultProps = {
    removeable: true
};

export default withStyles(styles)(IngredientEditor);
