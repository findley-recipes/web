import React from 'react';
import {Typography} from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
import InstructionStep from './InstructionStep';

const InstructionSection = props => {
    const {classes, instructions} = props;
    const steps = instructions.content.split('\n\n');
    const showStepNumbers = steps.length > 1;

    const content = steps.map((step, i) => (
        <InstructionStep key={i} number={i+1} showStepNumber={showStepNumbers}>
            {step}
        </InstructionStep>
    ));

    return (
        <div className={classes.root}>
            <Typography variant="title">
                Instructions
            </Typography>
            {content}
        </div>
    );
};

const styles = theme => ({
    root: {
        marginLeft: theme.spacing.unit,
        marginTop: theme.spacing.unit * 4
    },
});

export default withStyles(styles)(InstructionSection);
