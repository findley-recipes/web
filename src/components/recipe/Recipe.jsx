import React from 'react';
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';
import { Link } from 'react-router-dom';
import {Paper, Button} from '@material-ui/core';
import EditIcon from '@material-ui/icons/Edit';
import PrintIcon from '@material-ui/icons/Print';

import InstructionSection from './InstructionSection';
import IngredientSection from './IngredientSection';
import Notes from './Notes';
import UnderlineHeading from '../utils/UnderlineHeading';
import RecipeTagList from '../utils/RecipeTagList';
import CategoryIcon from '../utils/CategoryIcon';

const Recipe = props => {
    const {recipe, classes, authenticated} = props;

    return (
        <Paper className={classes.root}>
            <UnderlineHeading subTitle={recipe.author} rightMenu={() => (
                <CategoryIcon iconName={recipe.icon} />
            )}>
                {recipe.name}
            </UnderlineHeading>

            {recipe.ingredients.map((ingredients, i) => (
                <IngredientSection ingredients={ingredients} key={i} />
            ))}

            {recipe.instructions.map((instructions, i) => (
                <InstructionSection instructions={instructions} key={i} />
            ))}

            <Notes>{recipe.notes}</Notes>

            <div className={classes.footer}>
                <RecipeTagList tags={recipe.tags} link={true} />
                <div>
                    <Link to={`/recipes/${recipe.linkName}/print`}>
                        <Button className={classes.actionButton} size="small" variant="contained" color="secondary">
                            Print
                            <PrintIcon className={classes.actionIcon} />
                        </Button>
                    </Link>
                    {authenticated ? (
                    <Link to={`/recipes/${recipe.linkName}/edit`}>
                        <Button className={classes.actionButton} size="small" variant="contained" color="secondary">
                            Edit
                            <EditIcon className={classes.actionIcon} />
                        </Button>
                    </Link>
                    ) : null}
                </div>
            </div>
        </Paper>
    );
};

const styles = theme => ({
    root: {
        padding: theme.spacing.unit * 2,
        marginBottom: theme.spacing.unit * 4
    },
    footer: {
        display: 'flex',
        justifyContent: 'space-between',
        marginTop: theme.spacing.unit* 2
    },
    actionButton: {
        marginLeft: theme.spacing.unit,
    },
    actionIcon: {
        marginLeft: theme.spacing.unit,
        fontSize: 20
    }
});

export default connect(state => ({authenticated: state.authenticated}))(withStyles(styles)(Recipe));
