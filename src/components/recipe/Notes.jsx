import React from 'react';
import {Typography} from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';

const Notes = props => {
    const {classes, children} = props;

    if (!children) {
        return null;
    }

    return (
        <div className={classes.root}>
            <Typography variant="subheading" gutterBottom>
                Notes
            </Typography>
            <Typography variant="body1">
                {children}
            </Typography>
        </div>
    );
};

const styles = theme => ({
    root: {
        marginLeft: theme.spacing.unit,
        marginTop: theme.spacing.unit * 4
    },
});

export default withStyles(styles)(Notes);
