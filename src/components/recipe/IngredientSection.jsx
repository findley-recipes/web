import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import {Typography, Grid} from '@material-ui/core';

import Ingredient from './Ingredient';

const IngredientSection = props => {
    const {classes, ingredients} = props;

    return (
        <div className={classes.root}>
            <Typography variant="title" className={classes.nameText}>
                {ingredients.name === undefined ? 'Ingredients' : ingredients.name + ' Ingredients'}
            </Typography>

            <Grid container>
                {ingredients.list.map((ingredient, i) => (
                    <Grid item key={i} xs={12} sm={6} md={6} lg={6} xl={6}>
                        <Ingredient ingredient={ingredient} />
                    </Grid>
                ))}
            </Grid>
        </div>
    );
};

const styles = theme => ({
    root: {
        marginTop: theme.spacing.unit * 3,
        marginLeft: theme.spacing.unit
    },
    nameText: {
        marginBottom: theme.spacing.unit
    }
});

export default withStyles(styles)(IngredientSection);
