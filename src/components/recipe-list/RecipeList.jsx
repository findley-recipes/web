import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import {
    Paper,
    List,
    Typography,
} from '@material-ui/core';
import './recipe-list.css';
import RecipeListItem from './RecipeListItem';
import RecipeListItemLoader from './RecipeListItemLoader';

const styles = theme => ({
    root: {},
    header: {
        height: 48,
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginBottom: 0,
    },
    title: {
        marginLeft: theme.spacing.unit * 2,
        lineLeight: 48,
        [theme.breakpoints.down('xs')]: {
            marginLeft: theme.spacing.unit
        }
    }
});

const RecipeList = props => {
    const {classes, children, recipes, loading} = props;

    return (
        <div className={classes.root}>
            <div className={classes.header}>
                <Typography variant="headline" className={classes.title}>{props.header}</Typography>
                <div>
                    {children}
                </div>
            </div>
            <Paper>
                <List>
                    {renderItems(recipes, loading)}
                </List>
            </Paper>
        </div>
    );
};

function renderItems(recipes, loading) {
    if (loading) {
        return (
            <React.Fragment>
                <RecipeListItemLoader />
                <RecipeListItemLoader />
                <RecipeListItemLoader />
                <RecipeListItemLoader />
                <RecipeListItemLoader />
            </React.Fragment>
        );
    }

    return recipes.map((recipe, i) => (
        <RecipeListItem recipe={recipe} key={i} />
    ));
}

export default withStyles(styles)(RecipeList);
